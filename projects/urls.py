from projects.views import lists_projects, show_project, create_project
from django.urls import path

urlpatterns = [
    path("", lists_projects, name="list_projects"),
    path("<int:id>/", show_project, name="show_project"),
    path("create/", create_project, name="create_project"),
]
