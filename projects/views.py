from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


# Create your views here.
@login_required
def lists_projects(request):
    list_project = Project.objects.filter(owner=request.user)
    context = {"list_project": list_project}
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, id):
    project = Project.objects.get(id=id)
    context = {"project_id": project}
    return render(request, "projects/project_id.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm()
    context = {"form": form}
    return render(request, "projects/create_project.html", context)
