from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTaskForm
from tasks.models import Task


@login_required
def create_newtask(request):
    if request.method == "POST":
        form = CreateTaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTaskForm()
    context = {"form": form}
    return render(request, "tasks/create_newtask.html", context)


@login_required
def show_task(request):
    my_task = Task.objects.filter(assignee=request.user)
    context = {"task": my_task}
    return render(request, "tasks/my_task.html", context)
