from django.forms import ModelForm
from tasks.models import Task


class CreateTaskForm(ModelForm):
    class Meta:
        model = Task
        exclude = ["is_completed"]
