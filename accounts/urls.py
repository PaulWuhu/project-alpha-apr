from django.urls import path
from accounts.views import Log_in, log_out, sign_up

urlpatterns = [
    path("login/", Log_in, name="login"),
    path("logout/", log_out, name="logout"),
    path("signup/", sign_up, name="signup"),
]
